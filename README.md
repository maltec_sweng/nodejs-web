# nodejs-web

Install required packages
```
npm install mysql express express-session express-ejs-layouts ejs bcryptjs passport passport-local express-mysql-session minimist dayjs
```

If you want the server to automatically restart on file changes, install `nodemon`. It with require sudo:
```
npm install -g nodemon
```

Run your MySQL database and add correct credentials in `dbConnector.js`.

Run the server with
```
node app.js
```
or to get automatic restarts with `nodemon`
```
nodemon app.js
```

## API usage

The API is split into two; one for a user browser to access data for plotting graphs and one for monitoring servers to use for logging data.

### Browser API

The browser API is located at `http://serverhost:port/api/[monitoring server]`.
Authentication for this API makes use of the browser's session cookie.

#### GET `/activities`

Returns all activities for the monitoring server in the following style

```json
{
    "headers":[
        "id",
        "start",
        "end"
    ],
    "data":[
        {
            "id":1,
            "start":"2022-04-01T15:05:00.000Z",
            "end":"2022-04-01T16:35:00.000Z"
        },
        {
            "id":2,
            "start":"2022-04-02T14:45:32.000Z",
            "end":"2022-04-02T16:02:03.000Z"
        },
        {
            "id":3,
            "start":"2022-04-03T14:31:06.000Z",
            "end":"2022-04-03T17:02:03.000Z"
        }
    ]
}
```

#### GET `/activities/[from]`

Like above, but only since the given \[from\] date.
The date should have the format `YYYY-DD-MM HH:mm:ss`.

#### GET `/activities/[from]/[to]`

Like above, but between the given \[from\] and \[to\] dates.
The dates should have the format `YYYY-DD-MM HH:mm:ss`.


#### GET `/activity/[id]/events`

List all events for a given activity ID \[id\].

```json
{
    "headers":[
        "event_id",
        "activity_id",
        "event_type",
        "time"
    ],
    "data":[
        {
            "event_id":7,
            "activity_id":2,
            "event_type":"activate",
            "time":"2022-04-02T14:45:32.000Z"
        },
        {
            "event_id":8,
            "activity_id":2,
            "event_type":"leave",
            "time":"2022-04-02T15:00:57.000Z"
        },
        {
            "event_id":9,
            "activity_id":2,
            "event_type":"enter",
            "time":"2022-04-02T15:10:03.000Z"
        },
        {
            "event_id":10,
            "activity_id":2,
            "event_type":"deactivate",
            "time":"2022-04-02T16:02:03.000Z"
        }
    ]
}
```

#### POST `/events`

Get events for multiple activity IDs. Response format is like the one above.

The request must have the header `'Content-Type': 'application/json'`
and the body a list of activity IDs like `{"activity_ids":[1,2,3]}`.

### Monitoring server API

This API is available at `http://serverhost:port/api/ms`. It uses HTTP Basic Authorization.

That means any requests should have the header `'Authorization': 'Basic [credentials]'`.

The credentials have the following format `username:password`, which is then encoded using base64.

#### GET `/`

Returns `{"message":"success"}` if the request passed authorizarion.

#### POST `/activity/start`

Add a started activity. Returns the assigned activity ID.

The request must have the header `'Content-Type': 'application/json'`
and the body a start time like

```json
{
    "start_time": "2022-04-24 14:05:50"
}
```

#### POST `/activity/stop`

Stop an activity. Returns the activity ID on success.

The request must have the header `'Content-Type': 'application/json'`
and the body an activity ID and end time like

```json
{
    "id": 96,
    "end_time": "2022-04-24 15:15:43"
}
```

#### POST `/event/add`

Add an activity. Returns the assigned event ID.

The request must have the header `'Content-Type': 'application/json'`
and the body an activity ID and end time like

```json
{
    "activity_id": 96,
    "event_type": "activate",
    "time": "2022-04-24 14:05:50"
}
```
