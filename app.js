#!/usr/bin/env node
const express = require('express');     // Express web server framework
const router = express.Router();
const ejs = require('ejs');             // ejs is a templating engine
const db = require('./dbConnection');   // Wrapper for creatign and closing connections to the database
const expressEjsLayout = require('express-ejs-layouts')     // Express layout for ejs files
const args = require('minimist')(process.argv.slice(2));    // Parse runtime parameters
const session = require('express-session');
var MySQLStore = require('express-mysql-session')(session);
const passport = require('passport');
const db_options = require('./config/database')
const fs = require('fs');
const path = require('path');

require("./config/passport")(passport)

const app = express();

// Read app config if exists
const conf_path = 'config/app.json';
let app_config = {};
if (fs.existsSync(conf_path)) {
    console.log("Reading config");
    app_config = JSON.parse(fs.readFileSync(conf_path));
    console.log(app_config);
}

// If -p argument is given, use that port
// Otherwise if port is given in the configuration file, use that
// otherwise use 8080
let port = 8080; 
if (args.p) port = args.p;
else if (app_config['port']) port = app_config['port'];

//express session
var sessionStore = new MySQLStore(db_options)

app.use(session({
    key: 'kg_session',
    secret : 'YFk4oZCVeKwtZj',
    cookie: {
        // Stay logged in for 30 days
        maxAge: 30 /*days*/ * 24 /*h*/ * 60/*min*/ * 60/*s*/ * 1000/*ms*/
    },
    store: sessionStore,
    resave : true,
    saveUninitialized : true
}));
app.use(passport.initialize());
app.use(passport.session()); 

//EJS
app.set('view engine', 'ejs'); // Tell Express that we use ejs as our template engine
app.use(expressEjsLayout);

//BodyParser
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Serve static files from the 'public' directory
app.use('/', express.static('public'));

//Routes
app.use((req, res, next) => {
    res.locals.registration_enabled = true;
    if (app_config['allow_registration'] != undefined && !app_config['allow_registration']) res.locals.registration_enabled = false;
    next();
});

app.use('/', require('./routes/index'));
app.use('/users', require('./routes/users'));
app.use('/api/ms', require('./routes/api_ms'));
app.use('/api', require('./routes/api'));
app.use('/dashboard', require('./routes/dashboard'));

// Start the app
app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
});
