const path = require('path');
const fs = require('fs');
const fn = path.resolve(__dirname, './db.json');

const data = JSON.parse(fs.readFileSync(fn));

module.exports = data;