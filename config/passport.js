const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcryptjs');
const users = require("../models/user");

module.exports = function(passport) {
    passport.use(
        new LocalStrategy({usernameField : 'email'}, (email, password, done) => {
            // match user
            users.findone(email, function(user) {
                if (!user) {
                    console.log('Invalid email')
                    return done(null, false, {message: 'Invalid username or email'})
                };

                bcrypt.compare(password, user.hash, (err, isMatch) => {
                    console.log(`Comparing ${password} and ${user.hash} for`)
                    console.log(user);
                    if (err) throw err;


                    if (isMatch) return done(null, user)
                    else {
                        console.log('Invalid password')
                        return done(null, false, {message: 'Invalid username or email'})
                    };
                })
            })
        })
    )
    passport.serializeUser(function(user, done) {
        console.log('Serializing ' + user);
        done(null, user.email);
    });
    passport.deserializeUser(function(id, done) {
        console.log('Deserializing ' + id);
        users.findone(id, function(user) {
            let err;
            if (!user) err = new Error('Invalid user ID');
            done(err, user);
        })
    })
}