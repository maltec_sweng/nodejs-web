const db = require('./dbConnection')
const db_options = require('./config/database')
const activity = require('./models/activity')
const event = require('./models/event')

// Simple example of 3 activities over 3 days
const activities = [
    {
        // 1st of March
        start_time: new Date(2022, 03, 01, 17, 05, 00), 
        end_time: new Date(2022, 03, 01, 18, 35, 00),
        events: [
            {
                type: 'leave',
                time: new Date(2022, 03, 01, 17, 15, 00)
            },
            {
                type: 'enter',
                time: new Date(2022, 03, 01, 17, 30, 00)
            },
            {
                type: 'leave',
                time: new Date(2022, 03, 01, 17, 35, 00)
            },
            {
                type: 'enter',
                time: new Date(2022, 03, 01, 17, 50, 00)
            }
        ]
    },
    {
        // 2nd of March
        start_time: new Date(2022, 03, 02, 16, 45, 32),     
        end_time: new Date(2022, 03, 02, 18, 02, 03),       
        events: [                                           
            {
                type: 'leave',
                time: new Date(2022, 03, 02, 17, 00, 57)
            },
            {
                type: 'enter',
                time: new Date(2022, 03, 02, 17, 10, 03)
            }
        ]
    },
    {
        // 3rd of March
        start_time: new Date(2022, 03, 03, 16, 31, 06), 
        end_time: new Date(2022, 03, 03, 19, 02, 03),
        events: [
            {
                type: 'leave',
                time: new Date(2022, 03, 03, 17, 00, 57)
            },
            {
                type: 'alarm_start',
                time: new Date(2022, 03, 03, 17, 20, 57)
            },
            {
                type: 'enter',
                time: new Date(2022, 03, 03, 19, 01, 28)
            },
            {
                type: 'alarm_stop',
                time: new Date(2022, 03, 03, 19, 01, 29)
            }
        ]
    }
]

function main() {
    // Clone db options instead of creating a reference
    let opts = JSON.parse(JSON.stringify(db_options));
    opts.database = 'test_db';
    opts.multipleStatements = true;
    const con = db.openConnection(opts);
    let sql;

    sql = `DROP TABLE IF EXISTS activities;
DROP TABLE IF EXISTS events;
CREATE TABLE IF NOT EXISTS activities (id INT PRIMARY KEY, start_time DATETIME, end_time DATETIME);
CREATE TABLE IF NOT EXISTS events (event_id INT PRIMARY KEY, activity_id INT, event_type VARCHAR(16), time DATETIME);
`

    let aid = 1;
    let eid = 1;

    for (let act of activities) {
        let new_line = con.format("INSERT INTO activities (id, start_time, end_time) VALUES (?);\n", [[aid, act.start_time, act.end_time]])
        sql += new_line;

        new_line = con.format("INSERT INTO events (event_id, activity_id, event_type, time) VALUES (?);\n", [[eid, aid, 'activate', act.start_time]])
        sql += new_line;
        eid++;

        for (let ev of act.events) {
            let new_line = con.format("INSERT INTO events (event_id, activity_id, event_type, time) VALUES (?);\n", [[eid, aid, ev.type, ev.time]])
            sql += new_line;
            eid++;
        }
        new_line = con.format("INSERT INTO events (event_id, activity_id, event_type, time) VALUES (?);\n", [[eid, aid, 'deactivate', act.end_time]])
        sql += new_line;
        eid++;

        aid++;
    }

    console.log(sql)

    con.query(sql, (err, res) => {
        if (err) throw err;
        db.closeConnection(con);
    })
}

main();