const db = require('./dbConnection.js')            // Database class
const db_options = require('./config/database')    // Database configuration
const activity = require('./models/activity')
const event = require('./models/event')
const args = require('minimist')(process.argv.slice(2));    // Parse runtime parameters
const mon_server = require('./models/mon_srv')


/* Recreate inital database */
function recreate(after) {
    if (!after) after = function(){};

    let options = JSON.parse(JSON.stringify(db_options));
    let con = db.openConnection(options);
    let sql;

    db.ifTableExists('monitoring_servers', con, () => {
        sql = "SELECT * FROM monitoring_servers";
        con.query(sql, (err, res) => {
            let mslist = []
            for (row of res) {
                mslist.push(row.name)
            }
            drop_monitoring_servers(con, mslist, () => {
                sql = "DROP TABLE IF EXISTS monitoring_servers";
                con.query(sql, (err, res) => {
                    db.closeConnection(con);
                    after();
                })
            })
        })
    }, () => {
        db.closeConnection(con);
        after();
    })

}
 /**
  * 
  * @param {Connection} con 
  * @param {List} mslist 
  * @param {Function} after 
  */
function drop_monitoring_servers(con, mslist, after) {
    if (mslist.length === 0) {
        after();
    } else {
        let sql = "DROP DATABASE ??";
        let ms = mslist.pop()
        console.log('Deleting database ' + ms)
        con.query(sql, [ms], (err, res) => {
            drop_monitoring_servers(con, mslist, after)
        })
    }
}

/* T1 
Precondition:  Database is empty after following the setup guide [3]
Action:        The script for adding a monitoring server is run
Postcondition: A database and user for monitoring server has been created
*/
function t1() {
    recreate(() => {
        mon_server.newMonitoringServer('test', 'abcd1234', (succ) => {
            if (succ) {
                console.log("Successfully registered the test monitoring server")
            } else {
                console.log("Failed to regsiter the test monitoring server")
            }
        })
    });
}


/* T2 
Precondition:  Database is empty after following the setup guide [3] and a monitoring  server has been added
Action:        The script for adding a monitoring server is run
Postcondition: A database and user for monitoring server has been created alongside previous monitoring database
*/
function t2() {
    mon_server.newMonitoringServer('test2', 'abcd1234', (succ) => {
        if (succ) {
            console.log("Successfully registered the test monitoring server")
        } else {
            console.log("Failed to regsiter the test monitoring server")
        }
    })
}

/* T3 
Precondition:  Database is empty except for a monitoring server having been registered
Action:        An activity is added to the monitoring server’s database
Postcondition: The added activity is stored in the database
*/

/* T4 
Precondition:  Database has a monitoring server registered and an activity is stored in the corresponding database
Action:        An activity is added to the monitoring server’s database
Postcondition: The added activity is stored in the database alongside previous activities
*/

/* T5 
Precondition:  Database is empty except for a monitoring server having been registered
Action:        An event is added to the monitoring server’s database
Postcondition: The added event is stored in the database
*/

/* T6 
Precondition:  Database has a monitoring server registered and an event is stored in the corresponding database
Action:        An event is added to the monitoring server’s database
Postcondition: The added event is stored in the database alongside previous activities
*/


/* T7 
Precondition:  A monitoring server is added and no activities has been added
Action:        Read activities 
Postcondition: The read activities function returns an empty list
*/
function t7() {
    activity.findAll('test', (activities) => {
        if (activities.length === 0) {
            console.log("Success!");
        } else {
            console.log(`Failed! Found ${activities.length} activities:`);
            console.log(activities);
        }
    })
}

/* T8 
Precondition:  A monitoring server is added with multiple activities
Action:        Read activities
Postcondition: The read activities function returns a list of all activities
*/
function t8() {
    activity.findAll('test', (activities) => {
        if (activities.length !== 0) {
            console.log("Success!");
            console.log(activities);
        } else {
            console.log("Failed! Found no activities");
        }
    })
}

/* T9 
Precondition:  A monitoring server is added and no events has been added
Action:        Read events
Postcondition: The read events function returns an empty list
*/
function t9() {
    event.getEvents('test', 1, (events) => {
        if (events.length === 0) {
            console.log("Success!");
        } else {
            console.log(`Failed! Found ${events.length} events:`);
            console.log(events);
        }
    })
}

/* T10 
Precondition:  A monitoring server is added with multiple events added
Action:        Read events
Postcondition: The read events function returns a list of all events
*/
function t10() {
    event.getEvents('test', 1, (events) => {
        if (events.length === 0) {
            console.log("Failed!");
        } else {
            console.log(`Success! Found ${events.length} events:`);
            console.log(events);
        }
    })
}

if (args.r) {
    recreate();
}

if (args.t1) {
    t1();
}
if (args.t2) {
    t2();
}
if (args.t7) {
    t7();
}
if (args.t8) {
    t8();
}
if (args.t9) {
    t9();
}
if (args.t10) {
    t10();
}
