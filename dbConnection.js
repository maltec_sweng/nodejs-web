var mysql = require('mysql');

/**
 * Opens a connection to the database of which 
 * is described in the configuration options 
 * input as argument
 */
function openConnection(options) {
    var connection = mysql.createConnection(options);
    connection.connect(function (err) {
        if (err) throw err;
        //console.log("Connected!");
    });
    return connection;
}

/**
 * Close the given connection
 * @param {*} connection 
 */
function closeConnection(connection) {
    if (connection !== null) {
        //console.log("Ending connection");
        connection.end();
    }
}

/**
 * Returns a string of a reformatted date object 
 * in sql database
 * @param {*} date 
 * @returns string in UTC format
 */
function date_to_mysql_format(date) {
    //1998-01-23 12:45:56
    let dstr = date.getUTCFullYear() + '-' +
        (date.getUTCMonth()+1).toString().padStart(2, '0') + '-' +
        date.getUTCDate().toString().padStart(2, '0') + ' ' +
        date.getUTCHours().toString().padStart(2, '0') + ':' +
        date.getUTCMinutes().toString().padStart(2, '0') + ':' +
        date.getUTCSeconds().toString().padStart(2, '0');
    return dstr;
}

/**
 * Checks wether a table exists and prints the answer in the terminal
 * @param {*} table_name Name of the table you search as string
 * @param {*} con connection to database
 * @param {*} true_fn function for what will happen if the table exist
 * @param {*} false_fn function for what will happen if the table does not exist
 */
function ifTableExists(table_name, con, true_fn, false_fn) {
    let sql = mysql.format('SHOW TABLES LIKE ?', table_name);
    console.log(sql);
    con.query(sql, function(err, result) {                      // queries / asks whether the table exists  
        if (err) throw err;
        
        if (result.length < 1) {                                // If no tables with the given name are found, execute false_fn()
            console.log(`Table ${table_name} doesn't exist`);
            false_fn();
        } else {                                                // Else the table exists and true_fn() is executed.
            console.log(`Table ${table_name} exists`);
            true_fn();
        }
    })
}

module.exports = { openConnection, closeConnection, date_to_mysql_format, ifTableExists };