const db = require('../dbConnection.js')            // Database class
const db_options = require('../config/database')    // Database configuration

/**:dbname
 * 
 */
class Activity {
    constructor(id, start, end) {
        this.id = id;
        this.start = start;
        this.end = end;
    }
}

function findHelper(database_name, fn, additional_sql, values) {
    // Clone db options instead of creating a reference
    let options = JSON.parse(JSON.stringify(db_options));
    options.database = database_name;
    let con = db.openConnection(options);
    let sql;

    db.ifTableExists('activities', con, function () {
        // If the table exists, get all activity instances
        if (!additional_sql) additional_sql = "";
        if (!values) values = [];

        sql = con.format("SELECT * FROM activities " + additional_sql, values);
        console.log(sql);
        con.query(sql, function (err, result) {
            if (err) throw err;

            db.closeConnection(con);

            let activities = [];
            for (let activity of result) {
                // Create a new object for each instance
                activities.push(
                    new Activity(
                        activity.id,
                        activity.start_time,
                        activity.end_time
                    ));
            };
            // Call forward with the list of objects
            fn(activities);
        })
    }, function () {
        // If the table doesn't exists, call fn with an empty array.
        db.closeConnection(con);
        fn([]);
    });
}

function findAll(database_name, fn) {
    findHelper(database_name, fn);
}

function findSince(database_name, from, fn) {
    findHelper(database_name, fn, "WHERE start_time > ?", [db.date_to_mysql_format(from)]);
}

function findBetween(database_name, from, to, fn) {
    findHelper(database_name, fn, "WHERE start_time BETWEEN ? AND ?", [db.date_to_mysql_format(from), db.date_to_mysql_format(to)]);
}

/**
 * Add a new activity to the database
 */
function startActivity(start_time, database_name, fn) {
    // Clone db options instead of creating a reference
    let options = JSON.parse(JSON.stringify(db_options));
    options.database = database_name;
    let con = db.openConnection(options);
    let sql;

    sql = 'CREATE TABLE IF NOT EXISTS activities (id INT PRIMARY KEY, start_time DATETIME, end_time DATETIME)'
    con.query(sql, function (err, result) {
        if (err) throw err;
        sql = "SELECT * FROM activities ORDER BY id DESC LIMIT 1";
        con.query(sql, function (err, result) {
            if (err) throw err;

            let id = 1;
            if (result.length > 0) {
                id = result[0].id + 1;
            }

            sql = con.format("INSERT INTO activities (id, start_time) VALUES (?, ?)", [id, start_time]);
            con.query(sql, function (err, result) {
                if (err) throw err;

                db.closeConnection(con);
                fn(id);
            });
        });
    });
}

/**
 * Update an activity in the database with the ID of the given activity.
 */
function updateActivity(activity, database_name, fn) {
    // Clone db options instead of creating a reference
    let options = JSON.parse(JSON.stringify(db_options));
    options.database = database_name;
    let con = db.openConnection(options);
    let sql;

    console.log(activity);

    db.ifTableExists('activities', con, function () {
        if (activity.start_time || activity.end_time) {
            sql = con.format("UPDATE activities SET");
            if (activity.end_time) {
                sql += con.format(" end_time = ? ", [activity.end_time]);
            }
            if (activity.start_time) {
                sql += con.format(" start_time = ? ", [activity.start_time]);
            }
            sql += con.format("WHERE id = ?", [activity.id])

            console.log(sql);

            con.query(sql, function (err, result) {
                console.log(result);
                if (err) {
                    fn(-1);
                } else {
                    fn(activity.id)
                }
            });
        } else {
            fn(-1);
        }
    }, function () {
        fn(-1);
    });
}

/**
 * Adds an activity in the database. 
 */
function addActivity(activity, database_name, fn) {

}

module.exports = { Activity, findAll, findSince, findBetween, startActivity, updateActivity }