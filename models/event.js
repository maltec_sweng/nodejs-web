const db = require('../dbConnection.js')
const db_options = require('../config/database')

class Event {
    constructor(event_id, activity_id, event_type, time) {
        this.event_id = event_id                            // Event ID         - used for table searching
        this.activity_id = activity_id                      // Activity event ID - used to link event with activity 
        this.event_type = event_type                        // The type of event occurring
        this.time = time                                    // The time at which the event triggers
    }
}

function getEvents(database_name, activity_id, fn) {
    /*
    Get event data of event with given ID.
    */
    getMultiple(database_name, [activity_id], fn);
}

function getMultiple(database_name, ids, fn) {
    /*
    Get data from a list of event IDs.
    */
    console.log("Get " + ids);
    if (ids.length > 0) {
        // Clone db options instead of creating a reference
        let options = JSON.parse(JSON.stringify(db_options));
        options.database = database_name;
        let con = db.openConnection(options);
        let sql;

        db.ifTableExists('events', con, function () {
            // If the table exists, get all activity events for the given ID
            sql = 'SELECT * FROM events WHERE activity_id IN ?';
            con.query(sql, [[ids]], function (err, result) {
                if (err) throw err;

                let events = [];
                for (let ev of result) {
                    // Create a new object for each instance
                    events.push(new Event(
                        ev.event_id,
                        ev.activity_id,
                        ev.event_type,
                        ev.time
                    ));
                };
                // Call forward with the list of events
                fn(events);
            });
            db.closeConnection(con);

        }, function () {
            // If the table doesn't exists, call fn with an empty array.
            db.closeConnection(con);
            fn([]);
        });
    } else {
        console.log('events: No ids provided!');
        fn([]);
    }
}

function addEvent(event, database_name, fn) {
    // Clone db options instead of creating a reference
    let options = JSON.parse(JSON.stringify(db_options));
    options.database = database_name;
    let con = db.openConnection(options);
    let sql;

    sql = 'CREATE TABLE IF NOT EXISTS events (event_id INT PRIMARY KEY, activity_id INT, event_type VARCHAR(16), time DATETIME)'
    con.query(sql, function (err, result) {
        if (err) throw err;
        
        sql = "SELECT * FROM events ORDER BY event_id DESC LIMIT 1";
        con.query(sql, function (err, result) {
            if (err) throw err;

            let id = 1;
            if (result.length > 0) {
                id = result[0].event_id + 1;
            }

            sql = con.format("INSERT INTO events (event_id, activity_id, event_type, time) VALUES (?, ?, ?, ?)", [id, event.activity_id, event.event_type, event.time]);
            con.query(sql, function (err, result) {
                if (err) throw err;

                db.closeConnection(con);
                fn(id);
            });
        });
    });
}

module.exports = {Event, getEvents, getMultiple, addEvent}