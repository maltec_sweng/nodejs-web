const db = require('../dbConnection.js');            // Database class
const db_options = require('../config/database');    // Database configuration
const bcrypt = require('bcryptjs');

function exists(name, func) {
    let con = db.openConnection(db_options);
    let sql;

    sql = "SELECT * FROM monitoring_servers WHERE name = ?";
    
    con.query(sql, [name], function(err, result) {
        db.closeConnection(con);
        if (!err && result.length > 0) {
            res = result[0];
            func(res);
        } else {
            func(undefined);
        }
    })
};

function newMonitoringServer(name, password, done) {
    exists(name, (res) => {
        if (res) {
            console.log("There already exists a monitoring server with that name");
            done(undefined);
        }
        else {
            // hash password
            bcrypt.genSalt(10, (err, salt) =>
            bcrypt.hash(password, salt, (err, hash) => {
                if (err) throw err;
                let con = db.openConnection(db_options);

                // Create new MySQL user for monitoring server
                let sql = con.format("GRANT ALL PRIVILEGES ON ??.* to ??@'%' IDENTIFIED BY ?", [name, name, password]);
                console.log(sql);
                con.query(sql, (err, res) => {
                    if (err) throw err;

                    // Create database for monitoring server
                    con.query("CREATE DATABASE IF NOT EXISTS ??", [name], (err, res) => {
                        if (err) throw err;
                        console.log("Created database for monitoring server");

                        // Make sure monitoring servers tabe exists
                        con.query("CREATE TABLE IF NOT EXISTS monitoring_servers (name VARCHAR(16) PRIMARY KEY, hash VARCHAR(64))", (err, res) => {
                            if (err) throw err;

                            // Add monitoring server to table.
                            sql = `INSERT INTO monitoring_servers (name, hash) VALUES (?);`;
                            con.query(sql, [[name, hash]], (err, res) => {
                                if (err) throw err;
        
                                db.closeConnection(con);
                                done(res);
                            })
                        })
                    })
                });
            }));
        }
    })

}

function authMonitoringServer(name, password, done) {
    exists(name, (res) => {
        if (res) {
            bcrypt.compare(password, res.hash, (err, isMatch) => {
                if (err) throw err;
                done(isMatch);
            })
        }
    })
}

function list(done) {
    let con = db.openConnection(db_options);
    let sql = "SELECT name FROM monitoring_servers";
    con.query(sql, (err, res) => {
        db.closeConnection(con);
        if (res)
            done(res);
        else
            done([]);
    });
}

module.exports = {newMonitoringServer, authMonitoringServer, list}