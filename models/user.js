const db = require('../dbConnection.js')
const db_options = require('../config/database')
const bcrypt = require('bcryptjs');

class User {
    constructor(name, email, password_or_hash, date, is_hashed = false) {
        this.name = name;
        this.email = email;

        if (is_hashed) this.hash = password_or_hash
        else this.password = password_or_hash;

        if (date != undefined) this.date = date 
        else this.date = new Date();
    }

    save() {
        let con = db.openConnection(db_options);
        let sql;

        sql = "CREATE TABLE IF NOT EXISTS users (email VARCHAR(255) PRIMARY KEY, name VARCHAR(255) NOT NULL, passwordhash VARCHAR(255), date DATETIME NOT NULL)";
        con.query(sql, function (err, result) {
            if (err) throw err;
            console.log("Table created");
        });

        sql = "INSERT INTO users (email, name, passwordhash, date) VALUES (?,?,?,?)";
        let values = [this.email, this.name, this.hash, db.date_to_mysql_format(this.date)];
        con.query(sql, values, function (err, result) {
            if (err) throw err;
            console.log("Added user to table");
        });

        db.closeConnection(con);
    }
};

function findone(email, func) {
    let con = db.openConnection(db_options);
    let sql;

    db.ifTableExists('users', con, ()=>{
        sql = "SELECT * FROM users WHERE email = ?";
    
        con.query(sql, [email], function(err, result) {
            if (err) throw err;
            db.closeConnection(con);
            if (result.length > 0) {
                u = result[0];
                let user = new User(u.name, u.email, u.passwordhash, u.date, is_hashed=true);
                func(user);
            } else {
                func(undefined);
            }
        })
    }, ()=>{
        db.closeConnection(con);
        func(undefined);
    });
};

function newUser(email, name, password, func) {
    findone(email, (user)=>{
        if (user == undefined) {
            let u = new User(name, email, password);

            // hash password
            bcrypt.genSalt(10, (err, salt) =>
            bcrypt.hash(u.password, salt, (err, hash) => {
                if (err) func(false);

                u.hash = hash;
                u.save();
                func(true);
            }));
        } else {
            func(false);
        }
    });
}

module.exports = {User, findone, newUser}