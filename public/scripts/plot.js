const moment = window.moment;

/**
 * Get list of activities. 
 * If no times are given, return all activities. 
 * If both times are given, return activities between those times.
 * If only start time is given, return activities since that time.
 * @param {string} db Database name
 * @param {string} start optional - Start time (YYYY-MM-DDTHH:MM)
 * @param {string} stop optional - End time (YYYY-MM-DDTHH:MM)
 * @returns promise (activities)
 */
function get_activities(db, start, stop) {
    return new Promise((resolve, reject) => {
        let url = "/api/" + db + "/activities";
        if (start) {
            url += "/" + start;
            if (stop) {
                url += "/" + stop;
            }
        }
        fetch(url).then((response) => {
            response.json().then(responsedata => {
                
                let activities = [];
                for (let activity of responsedata.data) {
                    activities.push(activity);
                }
                resolve(activities);
            })
        })
    })
}

/**
 * Get events for a single activity
 * @param {string} db Database name
 * @param {int} activity_id Activity ID
 * @returns promise (events)
 */
function get_events(db, activity_id) {
    return new Promise((resolve, reject) => {
        let url = "/api/" + db + "/activity/" + activity_id + "/events";
        fetch(url).then((response) => {
            response.json().then(responsedata => {
                let events = [];
                for (let event of responsedata.data) {
                    events.push(event);
                }
                resolve(events);
            })
        })
    })
}

/**
 * Get events for a multiple activity
 * @param {string} db Database name
 * @param {list} activity_ids List of Activity IDs (int)
 * @returns promise (events)
 */
function get_events_multiple(db, activity_ids) {
    return new Promise((resolve, reject) => {
        let url = "/api/" + db + "/events";
        fetch(url, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            method: 'POST',
            body: '{"activity_ids":' + JSON.stringify(activity_ids) + '}'
        }).then((response) => {
            response.json().then(responsedata => {
                let events = [];
                for (let event of responsedata.data) {
                    events.push(event);
                }
                resolve(events);
            })
        })
    })
}


function create_chart_dataset(db, start_time, end_time) {
    console.log("Creating dataset from dataabse " + db + " from " + start_time + " to " + end_time);
    return new Promise((resolve, reject) => {
        get_activities(db, start_time, end_time).then((activities) => {
            let datasets = [];
            let ids = []; 
            for (let activity of activities) {
                ids.push(activity.id);
            }
            console.log(ids);
            get_events_multiple(db, ids).then((events) => {
                let activity_data = [];
                let precense_data = [];
                let alarm_data = [];

                activity_data.push({x:start_time, y:0});

                for (let event of events) {
                    let datapoint = {
                        x: new Date(event.time)
                    }
                    if (['activate', 'enter', 'alarm_start'].includes(event.event_type)) {
                        datapoint['y'] = true;
                    } else if (['deactivate', 'leave', 'alarm_stop'].includes(event.event_type)) {
                        datapoint['y'] = false;
                    } else {
                        datapoint['y'] = null;
                    }

                    // Add activate event to precense data, since the user is in the kitchen when the system activates
                    if (event.event_type == 'activate' || event.event_type == 'deactivate') {
                        activity_data.push(datapoint);
                        precense_data.push(datapoint);
                    } 
                    if (event.event_type == 'enter' || event.event_type == 'leave') {
                        precense_data.push(datapoint);
                    }
                    if (event.event_type == 'alarm_start' || event.event_type == 'alarm_stop') {
                        alarm_data.push(datapoint);
                    }
                }
                activity_data.push({x:end_time, y:0});

                datasets.push({
                    label: 'Activity',
                    data: activity_data,
                    backgroundColor: 'rgba(255, 206, 86, 0.2)',
                    borderColor: 'rgba(255, 206, 86, 1)',
                    borderWidth: 1,
                    fill: true,
                    stepped: true,
                });
                datasets.push({
                    label: 'Presence',
                    data: precense_data,
                    backgroundColor: 'rgba(54, 162, 235, 0.2)',
                    borderColor: 'rgba(54, 162, 235, 1)',
                    borderWidth: 1,
                    fill: true,
                    stepped: true,
                });
                datasets.push({
                    label: 'Alarm',
                    data: alarm_data,
                    backgroundColor: 'rgba(255, 99, 132, 0.2)',
                    borderColor: 'rgba(255, 99, 132, 1)',
                    borderWidth: 1,
                    fill: true,
                    stepped: true,
                });
                resolve(datasets);
            })
        })
    })
}

/**
 * 
 * @param {string} id ID of datetime selector to update
 * @param {int} delta Number to add to current time
 * @param {string} unit Unit of delta using moment.js units
 */
function set_datetime_selector(id, delta, unit) {
    document.getElementById(id).value = new moment().add(delta, unit).format("YYYY-MM-DDTHH:mm");
}

CHARTS = {};

/**
 * Initialize the chart
 * @param {string} chart_id 
 */
function init_chart(chart_id, db) {
    let c = new Chart(document.getElementById(chart_id), {
        type: 'line',
        data: {
            datasets: []
        },
        options: {
            scales : {
                x: {
                    type: 'time',
                    title: {
                        display: true,
                        text: 'Date'
                    }
                }
            },
            responsive: true,
        }
    });

    CHARTS[chart_id] = c;

    update_chart(chart_id, db);
}

/**
 * Update data on the chart
 * @param {string} chart_id 
 */
function update_chart(chart_id, db) {
    let c = CHARTS[chart_id];
    let start_time = moment(document.getElementById(chart_id+'_start-time').value);
    let end_time = moment(document.getElementById(chart_id+'_end-time').value);

    create_chart_dataset(db, 
        start_time.format("YYYY-MM-DDTHH:mm"),
        end_time.format("YYYY-MM-DDTHH:mm"))
    .then((datasets) => {
        console.log(datasets);
        c.data.datasets = datasets;
        c.update();
    }, (error) => {
        console.log("Error updating chart");
    });
}