const express = require('express');
const router = express.Router();
const activity = require('../models/activity');
const event = require('../models/event')

router.use((req, res, next) => {
    if (!req.user)
        res.status(401).send('Unauthorized');
    else
        next();
})

/**
 * Request a list of all activites from a database :dbname
 */
router.get('/:dbname/activities', (req, res) => {
    let result = {
        headers: ['id', 'start', 'end'],
        data: []
    }
    activity.findAll(req.params.dbname, function(activities) {
        result.data = activities;
        res.json(result);
    })
})
router.get('/:dbname/activities/:from', (req, res) => {
    let result = {
        headers: ['id', 'start', 'end'],
        data: []
    }
    activity.findSince(req.params.dbname, new Date(req.params.from), function(activities) {
        result.data = activities;
        res.json(result);
    })
})
router.get('/:dbname/activities/:from/:to', (req, res) => {
    let result = {
        headers: ['id', 'start', 'end'],
        data: []
    }
    activity.findBetween(req.params.dbname, new Date(req.params.from), new Date(req.params.to), function(activities) {
        result.data = activities;
        res.json(result);
    })
})
router.get('/:dbname/activity/:id/events', (req, res) => {
    let result = {
        headers: ['event_id', 'activity_id', 'event_type', 'time'],
        data: []
    }
    event.getEvents(req.params.dbname, req.params.id, function(events) {
        result.data = events;
        res.json(result);
    })
})
router.post('/:dbname/events', (req, res) => {
    let result = {
        headers: ['event_id', 'activity_id', 'event_type', 'time'],
        data: []
    }
    event.getMultiple(req.params.dbname, req.body.activity_ids, (events) => {
        result.data = events;
        res.json(result);
    })
})

/**
 * Post data to the server
 */
router.post('/:dbname/post', (req, res) => {
    // req.body should contain the received content
    
})

// Export the router instance so that it can be used in other files.
module.exports = router; 