const express = require('express');
const router = express.Router();
const activity = require('../models/activity');
const event = require('../models/event');
const ms = require('../models/mon_srv');

router.use((req, res, next) => {
    if (!req.headers.authorization || req.headers.authorization.indexOf('Basic ') === -1) {
        return res.status(401).json({message: 'Missing authorization content'});
    }

    // Basic Authorization headers is encoded 'username:password: as base64 
    const base64Credentials =  req.headers.authorization.split(' ')[1];
    const credentials = Buffer.from(base64Credentials, 'base64').toString('utf8');
    const [name, password] = credentials.split(':');
    ms.authMonitoringServer(name, password, (isMatch) => {
        if (isMatch) {
            res.locals.monitoring_server = name;
            next();
        } else {
            return res.status(401).json({message: 'Not authorized'});
        }
    });
});

router.get('/', (req, res) => {
    res.json({message: 'success'});
})

/**
 * Start an activity
 * Post body should be JSON formatted with a start_time value
 * 
 * Returns like 
 *  {id: X}
 * where X is the ID assigned to the new activity
 */
router.post('/activity/start', (req, res) => {
    activity.startActivity(req.body.start_time, res.locals.monitoring_server, (id) => {
        res.json({id: id})
    })
})

/**
 * End an activity
 * Post body should be JSON formatted with the ID of an activity and the end_time value
 * 
 * Returns like 
 *  {id: X}
 * where ID is the activity's ID if successful
 */
router.post('/activity/stop', (req, res) => {
    activity.updateActivity(
        {
            id: req.body.id,
            end_time: req.body.end_time,
        }, 
        res.locals.monitoring_server,
        (id) => {
            res.json({id: id})
        }
    )
})

/**
 * Create an event
 * Post body should be JSON with the following content
 *      activity_id, event_type, time
 * 
 * Returns like 
 *  {id: X}
 * where X is the ID assigned to the new event
 */
router.post('/event/add', (req, res) => {
    let new_event = {
        activity_id: req.body.activity_id, 
        event_type: req.body.event_type, 
        time: req.body.time
    }

    console.log(new_event)
    event.addEvent(new_event, res.locals.monitoring_server, (id) => {
        res.json({id: id})
    })
})

module.exports = router; 