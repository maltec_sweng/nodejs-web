const express = require('express');
const router = express.Router();
const activity = require('../models/activity');
const event = require('../models/event')
const ms = require('../models/mon_srv');

const dayjs = require('dayjs');
const customParseFormat = require('dayjs/plugin/customParseFormat');
dayjs.extend(customParseFormat);

router.use((req, res, next) => {
    if (!req.user)
        res.redirect('/users/login');
    else
        next();
})

router.get('/', (req, res) => {
    ms.list((mss) => {
        res.render('dashboard', {req:req, user:req.user, title: 'Dashboard', mss: mss});
    })
})

router.get('/:dbname/table', (req, res) => {
    if (!req.query.range || req.query.range == 1) {
        // All
        activity.findAll(req.params.dbname, function(instances) {
            let ids = [];
            console.log(instances);
            for (let instance of instances) {
                ids.push(instance.id);
            }
            event.getMultiple(req.params.dbname, ids, function(events) {
                console.log(events);
                res.render('table', {
                    req:req, 
                    user:req.user, 
                    title: 'Tables', 
                    instances: instances,
                    events: events,
                    dayjs:dayjs
                });
            })
        })
    } else if (req.query.range == 2) {
        // Since 1960
        activity.findSince(req.params.dbname, new Date(1960, 1, 1), function(instances) {
            let ids = [];
            for (let instance of instances) {
                ids.push(instance.id);
            }
            event.getMultiple(req.params.dbname, ids, function(events) {
                res.render('table', {
                    req:req, 
                    user:req.user, 
                    title: 'Tables', 
                    instances: instances,
                    events: events,
                    dayjs:dayjs
                });
            })
        })
    } else if (req.query.range == 3) {
        // Between 1890 and 1910
        activity.findBetween(req.params.dbname, new Date(1890, 1, 1), new Date(1910, 1, 1), function(instances) {
            let ids = [];
            for (let instance of instances) {
                ids.push(instance.id);
            }
            event.getMultiple(req.params.dbname, ids, function(events) {
                res.render('table', {
                    req:req, 
                    user:req.user, 
                    title: 'Tables', 
                    instances: instances,
                    events: events,
                    dayjs:dayjs
                });
            })
        })
    } else {
        res.render('table', {
            req:req, 
            user:req.user, 
            title: 'Tables', 
            instances: [],
            events: [],
            dayjs:dayjs
        });
    }
})

router.get('/:dbname/plots', (req, res) => {
    res.render('plots', {req:req, user:req.user, dayjs:dayjs, title:"Plots"})
})

// Export the router instance so that it can be used in other files.
module.exports = router; 