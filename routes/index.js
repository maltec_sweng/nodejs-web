const express = require('express');
const router = express.Router();

//login page -- When the user navigates to the root directory (performs a GET request) render the welcome.ejs page.
router.get('/', (req, res) => {
    if (req.user)
        res.redirect('/dashboard');
    else
        res.render('welcome', {req:req, title: 'Welcome'});
})

// Export the router instance so that it can be used in other files.
module.exports = router; 