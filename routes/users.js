const express = require('express');
const router = express.Router();
const users = require('../models/user');
const passport = require('passport');

router.get('/myaccount', (req, res) => {
    if (!req.user)
        res.redirect('/users/login');
    else {
        res.render('account', {req:req, user:req.user, title: 'My Account'});
    }
});

//login handle -- Handle the respective GET requests and render the appropriate pages
router.get('/login', (req, res) => {
    res.render('login', {req:req,title: 'Login'});
})
router.get('/register', (req, res) => {
    if (!res.locals.registration_enabled) {
        res.redirect('/users/login');
        return;
    }

    res.render('register', {req:req,title: 'Create account'})
})

//Register handle -- Handle the respective POST requests.
router.post('/register', (req, res) => {
    if (!res.locals.registration_enabled) {
        res.send(403);
        return;
    }

    const { name, email, password, password2 } = req.body;

    let errors = [];
    console.log('Name: ' + name + ', email: ' + email + ', pass: ' + password);
    if (!name || !email || !password || !password2) {
        errors.push({ msg: "Please fill in all fields" })
    }
    //check if match
    if (password !== password2) {
        errors.push({ msg: "Passwords don't match" });
    }
    //check if password is more than 6 characters
    if (password.length < 6) {
        errors.push({ msg: 'Password must be at least 6 characters' })
    }

    if (errors.length > 0) {
        res.render('register', {
            errors: errors,
            name: name,
            email: email,
            password: password,
            password2: password2
        })
    } else {
        // Validation pass
        users.findone(email, function (user) {
            console.log(user);
            if (user) {
                errors.push({ msg: "A user with this email is already registered" });
                res.render('register', {
                    errors: errors,
                    name: name,
                    email: email,
                    password: password,
                    password2: password2
                });
            } else {
                users.newUser(email, name, password, (succ) => {
                    if (succ) res.redirect('/users/login')
                    else {
                        errors.push({ msg: "Failed to register the user" });
                        res.render('register', {
                            errors: errors,
                            name: name,
                            email: email,
                            password: password,
                            password2: password2
                        });
                    }
                })
            }
        })
    }
})
router.post('/login', (req, res, next) => {
    passport.authenticate('local', {
        successRedirect: '/dashboard',
        failureRedirect: '/users/login'
    })(req, res, next);
})

//logout
router.get('/logout', (req, res) => {
    req.logout();
    //req.flash('success_msg','Now logged out');
    res.redirect('/'); 
})
module.exports = router;