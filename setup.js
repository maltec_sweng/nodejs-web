#!/usr/bin/env node
const db = require('./dbConnection');
const ms = require('./models/mon_srv');
const user = require('./models/user');
const fs = require('fs');
const dbConfFile = './config/db.json';
let db_options = {}; // = require('./config/database');

const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
});

if (process.argv.includes('--first-time')) {
    console.log('Please enter your database settings:')
    readline.question('Host (localhost)> ', (host) => {
        readline.question('User (webservice)> ', (user) => {
            readline.question('Password> ', (password) => {
                readline.question('Database (kitchenguard)> ', (database) => {
                    db_options.host = host === "" ? "localhost" : host;
                    db_options.user = user === "" ? "webservice" : user;
                    db_options.password = password;

                    // Connect before adding the database to options
                    let con = db.openConnection(db_options); 
                    db_options.database = database === "" ? "kitchenguard" : database;

                    fs.writeFileSync(dbConfFile, JSON.stringify(db_options, null, 4));

                    let sql = con.format("CREATE DATABASE IF NOT EXISTS ??", [db_options.database]);
                    console.log(sql);
                    con.query(sql, (err, result) => {
                        if (err) {
                            console.log(err);
                        } else {
                            console.log(result)
                            console.log('Database created');
                        }
                        db.closeConnection(con);
                        readline.close();
                    });
                });
            });
        });
    });
} else if (process.argv.includes('--new-monitoring-server')) {
    readline.question("Name> ", (name) => {
        if (name.length > 0 && name.length < 16)
            readline.question("Password> ", (pass) => {
                if (pass.length >= 8) {
                    ms.newMonitoringServer(name, pass, (succ) => {
                        if (succ) {
                            console.log("Successfully registered the monitoring server")
                        } else {
                            console.log("Failed to regsiter the monitoring server")
                        }
                        readline.close();
                    })
                } else {
                    console.log("The password should be at least 8 chars");
                    readline.close();
                }
            })
        else { // name of invalid length
            console.log("The name must be between 1 and 16 characters long");
            readline.close();
        }
    })
} else if (process.argv.includes('--new-user')) {
    readline.question("Email> ", (email) => {
        readline.question("Name> ", (name) => {
            readline.question("Password> ", (pass) => {
                if (pass.length >= 8) {
                    user.newUser(email, name, pass, (succ) => {
                        if (succ) {
                            console.log("Successfully registered the user")
                        } else {
                            console.log("Failed to regsiter the user")
                        }
                        readline.close();
                    })
                } else {
                    console.log("The password should be at least 8 chars");
                    readline.close();
                }
            })
        })
    })
} else {
    readline.close();
}
