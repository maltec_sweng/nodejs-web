from test_base import *
from datetime import datetime, timedelta

def add_activity():
    res = send_api_request(
        '/activity/start', 
        {
            'start_time': datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
        })
    send_api_request(
        '/activity/stop',
        {
            'id': res['id'],
            'end_time': (datetime.utcnow() + timedelta(0,60*2)).strftime('%Y-%m-%d %H:%M:%S')
        }
    )


if __name__ == "__main__":
    add_activity()

