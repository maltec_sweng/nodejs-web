from test_base import *
from datetime import datetime

def add_event(event):
    res = send_api_request(
        '/event/add', 
        {
            'activity_id': 1,
            'event_type': event,
            'time': datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
        })

if __name__ == '__main__':
    add_event('leave')