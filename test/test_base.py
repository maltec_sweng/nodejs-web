import requests
import base64
import json

def send_api_request(url: str, data: dict = None):
    """
    Creates connection to the webservice, and either gets or posts requests.
    
    Returns:
        dict: a dict of the request data
    """
    credentials_plain = f'test:abcd1234'
    cred_encoded = base64.b64encode(bytes(credentials_plain, 'utf8')).decode("utf8")

    headers = {'Authorization': f'Basic {cred_encoded}'}

    # Compose the url
    full_url = 'http://localhost:8080/api/ms'
    full_url += url
    
    if data is None:
        # GET request
        res = requests.get(full_url, headers=headers)
        return res.json()
    else:
        # POST requests
        print(f"Sending POST request to {full_url} with data {data}")
        
        headers['Content-Type'] = 'application/json'
        json_data = json.dumps(data)
        res = requests.post(full_url, json_data, headers=headers)
        print(f"Received {res}")
        print(res.json())
        return res.json()
